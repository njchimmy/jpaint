package main;

import controller.IJPaintController;
import controller.JPaintController;
import model.*;
import model.Rectangle;
import model.interfaces.ICommand;
import model.persistence.ApplicationState;
import view.gui.Gui;
import view.gui.GuiWindow;
import view.gui.PaintCanvas;
import view.interfaces.IGuiWindow;
import view.interfaces.IUiModule;

// MY IMPORTS
import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;


public class Main {
    public static void main(String[] args) {
        PaintCanvas canvas = new PaintCanvas(); // only create one paint canvas
        IGuiWindow guiWindow = new GuiWindow(canvas);
        IUiModule uiModule = new Gui(guiWindow);
        ApplicationState appState = new ApplicationState(uiModule);

        ShapeList masterShapeList = new ShapeList();
        ShapeList selectedShapeList = new ShapeList();
        ShapeList clipboardShapeList = new ShapeList();
        ShapeDrawer shapeDrawer = new ShapeDrawer(canvas);
        ShapeListManager shapeListManager = new ShapeListManager(masterShapeList, selectedShapeList, clipboardShapeList);
        clickHandler clickHandler = new clickHandler(appState, shapeListManager);

        IJPaintController controller = new JPaintController(uiModule, appState, shapeListManager);

        controller.setup();
        masterShapeList.registerObserver(shapeDrawer);
        canvas.addMouseListener(clickHandler);
    }
}