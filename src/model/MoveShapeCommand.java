package model;

import model.interfaces.ICommand;
import model.interfaces.IShape;
import model.interfaces.IUndoable;

public class MoveShapeCommand implements ICommand, IUndoable {
    private ShapeList masterShapeList;
    private ShapeList selectedShapeList;
    private int deltaX;
    private int deltaY;

    public MoveShapeCommand(ShapeListManager shapeListManager, Pair moveStartPair, Pair moveEndPair) {
        this.masterShapeList = shapeListManager.getMasterShapeList();
        this.selectedShapeList = shapeListManager.getSelectedShapeList();
        this.deltaX = moveEndPair.getX() - moveStartPair.getX();
        this.deltaY = moveEndPair.getY() - moveStartPair.getY();
    }

    public void run() {
        moveShapes();
        CommandHistory.add(this);
        System.out.println("Move command added to Command History");
    }

    public void undo() {
        if (!selectedShapeList.getShapeList().isEmpty()) {
            int count = 0;
            for (IShape s : selectedShapeList.getShapeList()) {
                Pair oldStartPair = s.getStartPair();
                Pair oldEndPair = s.getEndPair();
                s.setStartPair(new Pair(oldStartPair.getX() - deltaX,
                        oldStartPair.getY() - deltaY));
                s.setEndPair(new Pair(oldEndPair.getX() - deltaX,
                        oldEndPair.getY() - deltaY));
                count++;
            }
            masterShapeList.shapesMoved();
            System.out.println("Number of shapes moved: " + count);
        }
//        else {
//            System.out.println("No shapes selected to move!");
//        }
    }

    public void redo() {
        moveShapes();
    }

    private void moveShapes() {
        if (!selectedShapeList.getShapeList().isEmpty()) {
            int count = 0;
            for (IShape s : selectedShapeList.getShapeList()) {
                Pair oldStartPair = s.getStartPair();
                Pair oldEndPair = s.getEndPair();
                s.setStartPair(new Pair(oldStartPair.getX() + deltaX,
                        oldStartPair.getY() + deltaY));
                s.setEndPair(new Pair(oldEndPair.getX() + deltaX,
                        oldEndPair.getY() + deltaY));
                count++;
            }
            masterShapeList.shapesMoved();
            System.out.println("Number of shapes moved: " + count);
        }
//        else {
//            System.out.println("No shapes selected to move!");
//        }
    }
}
