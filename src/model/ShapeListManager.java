package model;

public class ShapeListManager {
    private ShapeList masterShapeList;
    private ShapeList selectedShapeList;
    private ShapeList clipboardShapeList;

    public ShapeListManager(ShapeList masterShapeList, ShapeList selectedShapeList, ShapeList clipboardShapeList) {
        this.masterShapeList = masterShapeList;
        this.selectedShapeList = selectedShapeList;
        this.clipboardShapeList = clipboardShapeList;

    }

    public ShapeList getMasterShapeList() {
        return masterShapeList;
    }

    public ShapeList getSelectedShapeList() {
        return selectedShapeList;
    }

    public ShapeList getClipboardShapeList() { return clipboardShapeList; }
}
