package model;

import model.ShapeList;
import model.ShapeListManager;
import model.interfaces.ICommand;
import model.interfaces.IShape;

public class CopyCommand implements ICommand {
    private ShapeList selectedShapeList;
    private ShapeList clipboardShapeList;


    public CopyCommand(ShapeListManager shapeListManager) {
        this.selectedShapeList = shapeListManager.getSelectedShapeList();
        this.clipboardShapeList = shapeListManager.getClipboardShapeList();
    }

    @Override
    public void run() {
        for (IShape s : selectedShapeList.getShapeList()) {
            clipboardShapeList.add(s.deepCopy());
        }
        System.out.println("clipboardShapeList size: " + clipboardShapeList.getShapeList().size());
    }
}
