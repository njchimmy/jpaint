package model;

import model.interfaces.IShape;
import model.interfaces.Observer;
import model.interfaces.Subject;

import java.util.ArrayList;

/*
* MY CLASS, MY CODE, NONE FROM ORIGINAL PROJECT SRC
* Create a ShapeList collection class that houses a list of shapes.
* This ShapeList will be used to store the data for what is displayed on the screen.
 */

public class ShapeList implements Subject {

    public ArrayList<IShape> shapeList;
    private ArrayList<Observer> observers;

    public ShapeList() {
        shapeList = new ArrayList<>();
        observers = new ArrayList<>();
    }

    public void add(IShape shape) {
        shapeList.add(shape);
        notifyObservers();
    }

    public void remove() {
        // Undo DrawShapeCommand
        shapeList.remove(shapeList.size()-1);
        notifyObservers();
    }

    public void shapesMoved() {
        System.out.println("shapesMoved called");
        notifyObservers();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer o : observers) {
            o.update(shapeList);
        }
    }

    public ArrayList<IShape> getShapeList() {
        return shapeList;
    }
}
