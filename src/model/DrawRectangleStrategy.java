package model;

import model.interfaces.IDrawShapeStrategy;
import model.interfaces.IShape;

import java.awt.*;

public class DrawRectangleStrategy implements IDrawShapeStrategy {

    DrawRectangleStrategy() {
    }

    @Override
    public void draw(Graphics2D graphics2D, IShape rectangle) {
        // TO DO
        int width = rectangle.getEndPair().getX() - rectangle.getStartPair().getX();
        int height = rectangle.getEndPair().getY() - rectangle.getStartPair().getY();
        int xPosition = rectangle.getStartPair().getX();
        int yPosition = rectangle.getStartPair().getY();

        switch (rectangle.getShapeConfiguration().getActiveShapeShadingType()) {
            case OUTLINE_AND_FILLED_IN:
                graphics2D.setColor(rectangle.getShapeConfiguration().getActiveSecondaryColor().getColor());
                graphics2D.fillRect(xPosition, yPosition, width, height);
                graphics2D.setColor(rectangle.getShapeConfiguration().getActivePrimaryColor().getColor());
                graphics2D.setStroke(new BasicStroke(3));
                graphics2D.drawRect(xPosition, yPosition, width, height);
            case OUTLINE:
                graphics2D.setColor(rectangle.getShapeConfiguration().getActivePrimaryColor().getColor());
                graphics2D.setStroke(new BasicStroke(3));
                graphics2D.drawRect(xPosition, yPosition, width, height);
            default:
                graphics2D.setColor(rectangle.getShapeConfiguration().getActiveSecondaryColor().getColor());
                graphics2D.fillRect(xPosition, yPosition, width, height);
        }
    }
}
