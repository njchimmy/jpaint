package model;

import java.awt.*;

public class ShapeConfiguration {
    private ShapeType activeShapeType;
    private ColorAdapter activePrimaryColor;
    private ColorAdapter activeSecondaryColor;
    private ShapeShadingType activeShapeShadingType;
    private StartAndEndPointMode activeStartAndEndPointMode;

    public ShapeConfiguration(ShapeType activeShapeType, ColorAdapter activePrimaryColor,
                              ColorAdapter activeSecondaryColor, ShapeShadingType activeShapeShadingType,
                              StartAndEndPointMode activeStartAndEndPointMode) {
        this.activeShapeType = activeShapeType;
        this.activePrimaryColor = activePrimaryColor;
        this.activeSecondaryColor = activeSecondaryColor;
        this.activeShapeShadingType = activeShapeShadingType;
        this.activeStartAndEndPointMode = activeStartAndEndPointMode;
    }

    public ShapeType getActiveShapeType() {
        return activeShapeType;
    }

    public ColorAdapter getActivePrimaryColor() {
        return activePrimaryColor;
    }

    public ColorAdapter getActiveSecondaryColor() {
        return activeSecondaryColor;
    }

    public ShapeShadingType getActiveShapeShadingType() {
        return activeShapeShadingType;
    }

    public StartAndEndPointMode getActiveStartAndEndPointMode() {
        return activeStartAndEndPointMode;
    }
}
