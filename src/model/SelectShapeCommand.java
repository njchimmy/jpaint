package model;

import model.interfaces.ICommand;
import model.interfaces.IShape;

public class SelectShapeCommand implements ICommand {
    private ShapeList masterShapeList;
    private ShapeList selectedShapeList;
    private Pair selectionStartPair;
    private Pair selectionEndPair;

    public SelectShapeCommand(ShapeListManager shapeListManager, Pair selectionStartPair, Pair selectionEndPair) {
        this.masterShapeList = shapeListManager.getMasterShapeList();
        this.selectedShapeList = shapeListManager.getSelectedShapeList();
        this.selectionStartPair = selectionStartPair;
        this.selectionEndPair = selectionEndPair;
    }

    public void run() {
//        int count = 0;
        for(IShape s : masterShapeList.getShapeList()) {
            if (CollisionDetector.detectCollision(s, selectionStartPair, selectionEndPair)) {
                selectedShapeList.add(s);
//                count++;
            }
        }
//        System.out.println("Shape selected! " + count);
//        if (selectedShapeList.getShapeList().isEmpty()){
//            System.out.println("No shapes selected!");
//        }
    }
}
