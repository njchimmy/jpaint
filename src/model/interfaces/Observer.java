package model.interfaces;

import model.interfaces.IShape;

import java.util.ArrayList;

public interface Observer {
    void update(ArrayList<IShape> shapeList);
}
