package model.interfaces;

import model.interfaces.IShape;

import java.awt.*;

public interface IDrawShapeStrategy {
    void draw(Graphics2D graphics2D, IShape s);
}
