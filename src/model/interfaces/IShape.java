package model.interfaces;

import model.Pair;
import model.ShapeConfiguration;

import java.awt.*;

public interface IShape {
    Pair getStartPair();

    Pair getEndPair();

    ShapeConfiguration getShapeConfiguration();

    void setStartPair(Pair newStartPair);

    void setEndPair(Pair newEndPair);

    IShape deepCopy();
}


