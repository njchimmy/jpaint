package model;

import model.interfaces.IDrawShapeStrategy;

public class DrawShapeStrategyFactory {

    public static IDrawShapeStrategy DrawRectangleStrategy() {
        return new DrawRectangleStrategy();
    }

    public static IDrawShapeStrategy DrawEllipseStrategy() {
        return new DrawEllipseStrategy();
    }

    public static IDrawShapeStrategy DrawTriangleStrategy() {
        return new DrawTriangleStrategy();
    }
}
