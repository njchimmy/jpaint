package model;

//The ShapeDrawer class will iterate over each shape and will draw the shape on the canvas. 

import model.interfaces.IDrawShapeStrategy;
import model.interfaces.IShape;
import model.interfaces.Observer;
import view.gui.PaintCanvas;

import java.awt.*;
import java.util.ArrayList;

public class ShapeDrawer implements Observer {
    PaintCanvas canvas;

    public ShapeDrawer(PaintCanvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public void update(ArrayList<IShape> shapeList) {
        // PAINT WHITE RECTANGLE OVER CANVAS
        Graphics2D whiteCanvas = canvas.getGraphics2D();
        whiteCanvas.setColor(Color.WHITE);
        whiteCanvas.fillRect(0, 0, 1280, 800);
        // REDRAW CANVAS
        for(IShape s: shapeList) {
            // USES STRATEGY FACTORY
            IDrawShapeStrategy drawStrategy;
            switch(s.getShapeConfiguration().getActiveShapeType()) {
                case ELLIPSE:
                    drawStrategy = DrawShapeStrategyFactory.DrawEllipseStrategy();
                    break;
                case RECTANGLE:
                    drawStrategy = DrawShapeStrategyFactory.DrawRectangleStrategy();
                    break;
                case TRIANGLE:
                    drawStrategy = DrawShapeStrategyFactory.DrawTriangleStrategy();
                    break;
                default:
                    throw new Error("Error in ShapeDrawer behavior class");
            }
            drawStrategy.draw(canvas.getGraphics2D(), s);
        }
    }
}
