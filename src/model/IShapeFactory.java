package model;

import model.interfaces.IShape;

public class IShapeFactory {
    private IShapeFactory() {}

    public static IShape createEllipse(Pair startPair, Pair endPair, ShapeConfiguration shapeConfiguration) {
        return new Ellipse(startPair, endPair, shapeConfiguration);
    }

    public static IShape createRectangle(Pair startPair, Pair endPair, ShapeConfiguration shapeConfiguration) {
        return new Rectangle(startPair, endPair, shapeConfiguration);
    }

    public static IShape createTriangle(Pair startPair, Pair endPair, ShapeConfiguration shapeConfiguration) {
        return new Triangle(startPair, endPair, shapeConfiguration); }
}
