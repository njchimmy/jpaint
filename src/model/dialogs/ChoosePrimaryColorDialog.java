package model.dialogs;

import model.ColorAdapter;
import model.ShapeColor;
import model.interfaces.IApplicationState;
import view.interfaces.IDialogChoice;

import java.awt.*;
import java.util.ArrayList;

public class ChoosePrimaryColorDialog implements IDialogChoice<ColorAdapter> {

    private final IApplicationState applicationState;
    private final ArrayList<ColorAdapter> primaryColors;

    public ChoosePrimaryColorDialog(IApplicationState applicationState) {
        this.applicationState = applicationState;
        primaryColors = new ArrayList<ColorAdapter>();
        primaryColors.add(new ColorAdapter(Color.BLACK, ShapeColor.BLACK));
        primaryColors.add(new ColorAdapter(Color.BLUE, ShapeColor.BLUE));
        primaryColors.add(new ColorAdapter(Color.CYAN, ShapeColor.CYAN));
        primaryColors.add(new ColorAdapter(Color.DARK_GRAY, ShapeColor.DARK_GRAY));
        primaryColors.add(new ColorAdapter(Color.GRAY, ShapeColor.GRAY));
        primaryColors.add(new ColorAdapter(Color.GREEN, ShapeColor.GREEN));
        primaryColors.add(new ColorAdapter(Color.LIGHT_GRAY, ShapeColor.LIGHT_GRAY));
        primaryColors.add(new ColorAdapter(Color.MAGENTA, ShapeColor.MAGENTA));
        primaryColors.add(new ColorAdapter(Color.ORANGE, ShapeColor.ORANGE));
        primaryColors.add(new ColorAdapter(Color.PINK, ShapeColor.PINK));
        primaryColors.add(new ColorAdapter(Color.RED, ShapeColor.RED));
        primaryColors.add(new ColorAdapter(Color.WHITE, ShapeColor.WHITE));
        primaryColors.add(new ColorAdapter(Color.YELLOW, ShapeColor.YELLOW));
    }

    @Override
    public String getDialogTitle() {
        return "Primary Color";
    }

    @Override
    public String getDialogText() {
        return "Select a primary color from the menu below:";
    }

    @Override
    public ColorAdapter[] getDialogOptions() {
        ColorAdapter[] primaryColorAdapters = new ColorAdapter[primaryColors.size()];
        return primaryColors.toArray(primaryColorAdapters);
    }

    @Override
    public ColorAdapter getCurrentSelection() {
        return applicationState.getActivePrimaryColor();
    }
}
