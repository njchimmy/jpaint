package model.dialogs;

import model.ColorAdapter;
import model.ShapeColor;
import model.interfaces.IApplicationState;
import view.interfaces.IDialogChoice;

import java.awt.*;
import java.util.ArrayList;

public class ChooseSecondaryColorDialog implements IDialogChoice<ColorAdapter> {

    private final IApplicationState applicationState;
    private final ArrayList<ColorAdapter> secondaryColors;

    public ChooseSecondaryColorDialog(IApplicationState applicationState) {
        this.applicationState = applicationState;
        secondaryColors = new ArrayList<ColorAdapter>();
        secondaryColors.add(new ColorAdapter(Color.BLACK, ShapeColor.BLACK));
        secondaryColors.add(new ColorAdapter(Color.BLUE, ShapeColor.BLUE));
        secondaryColors.add(new ColorAdapter(Color.CYAN, ShapeColor.CYAN));
        secondaryColors.add(new ColorAdapter(Color.DARK_GRAY, ShapeColor.DARK_GRAY));
        secondaryColors.add(new ColorAdapter(Color.GRAY, ShapeColor.GRAY));
        secondaryColors.add(new ColorAdapter(Color.GREEN, ShapeColor.GREEN));
        secondaryColors.add(new ColorAdapter(Color.LIGHT_GRAY, ShapeColor.LIGHT_GRAY));
        secondaryColors.add(new ColorAdapter(Color.MAGENTA, ShapeColor.MAGENTA));
        secondaryColors.add(new ColorAdapter(Color.ORANGE, ShapeColor.ORANGE));
        secondaryColors.add(new ColorAdapter(Color.PINK, ShapeColor.PINK));
        secondaryColors.add(new ColorAdapter(Color.RED, ShapeColor.RED));
        secondaryColors.add(new ColorAdapter(Color.WHITE, ShapeColor.WHITE));
        secondaryColors.add(new ColorAdapter(Color.YELLOW, ShapeColor.YELLOW));
    }

    @Override
    public String getDialogTitle() {
        return "Secondary Color";
    }

    @Override
    public String getDialogText() {
        return "Select a secondary color from the menu below:";
    }

    @Override
    public ColorAdapter[] getDialogOptions() {
        ColorAdapter[] secondaryColorAdapters = new ColorAdapter[secondaryColors.size()];
        return secondaryColors.toArray(secondaryColorAdapters);
    }

    @Override
    public ColorAdapter getCurrentSelection() {
        return applicationState.getActiveSecondaryColor();
    }
}
