package model;

import model.interfaces.IDrawShapeStrategy;
import model.interfaces.IShape;

import java.awt.*;

public class DrawEllipseStrategy implements IDrawShapeStrategy {

    DrawEllipseStrategy() {
    }

    @Override
    public void draw(Graphics2D graphics2D, IShape ellipse) {
        // TO DO
        int xPosition = ellipse.getStartPair().getX();
        int yPosition = ellipse.getStartPair().getY();
        int width = ellipse.getEndPair().getX() - xPosition;
        int height = ellipse.getEndPair().getY() - yPosition;

        switch (ellipse.getShapeConfiguration().getActiveShapeShadingType()) {
            case OUTLINE_AND_FILLED_IN:
                graphics2D.setColor(ellipse.getShapeConfiguration().getActiveSecondaryColor().getColor());
                graphics2D.fillOval(xPosition, yPosition, width, height);
                graphics2D.setColor(ellipse.getShapeConfiguration().getActivePrimaryColor().getColor());
                graphics2D.setStroke(new BasicStroke(3));
                graphics2D.drawOval(xPosition, yPosition, width, height);
                break;
            case OUTLINE:
                graphics2D.setColor(ellipse.getShapeConfiguration().getActivePrimaryColor().getColor());
                graphics2D.setStroke(new BasicStroke(5));
                graphics2D.drawOval(xPosition, yPosition, width, height);
                break;
            default:    // FILLED_IN
                graphics2D.setColor(ellipse.getShapeConfiguration().getActiveSecondaryColor().getColor());
                graphics2D.fillOval(xPosition, yPosition, width, height);
        }
    }
}
