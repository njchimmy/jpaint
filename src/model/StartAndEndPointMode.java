package model;

// what happens when you click and drag on the canvas

public enum StartAndEndPointMode {
    DRAW,
    SELECT,
    MOVE
}
