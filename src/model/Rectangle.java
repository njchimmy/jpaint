package model;

import model.interfaces.IShape;

import java.awt.*;

public class Rectangle implements IShape {

    private Pair startPair;
    private Pair endPair;
    private ShapeConfiguration shapeConfiguration;

    public Rectangle(Pair startPair, Pair endPair, ShapeConfiguration shapeConfiguration) {
        this.startPair = startPair;
        this.endPair = endPair;
        this.shapeConfiguration = shapeConfiguration;
    }

    @Override
    public Pair getStartPair() {
        return startPair;
    }

    @Override
    public Pair getEndPair() {
        return endPair;
    }

    @Override
    public ShapeConfiguration getShapeConfiguration() {
        return shapeConfiguration;
    }

    @Override
    public void setStartPair(Pair startPair) {
        this.startPair = startPair;
    }

    @Override
    public void setEndPair(Pair endPair) {
        this.endPair = endPair;
    }

    @Override
    public IShape deepCopy() {
        return new Rectangle(this.startPair, this.endPair, this.shapeConfiguration);
    }
}
