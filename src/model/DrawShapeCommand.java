package model;

import model.interfaces.ICommand;
import model.interfaces.IShape;
import model.interfaces.IUndoable;
import model.persistence.ApplicationState;

import java.awt.*;

public class DrawShapeCommand implements ICommand, IUndoable {
    private ShapeList masterShapeList;
    private ApplicationState appState;
    private Pair startPair;
    private Pair endPair;
    private IShape lastShapeDrawn;

    public DrawShapeCommand(ShapeList masterShapeList, ApplicationState appState, Pair startPair, Pair endPair) {
        this.masterShapeList = masterShapeList;
        this.appState = appState;
        this.startPair = startPair;
        this.endPair = endPair;
    }

    @Override
    public void run() {
        ShapeConfiguration shapeConfiguration = appState.getCurrentShapeConfiguration();

        switch (shapeConfiguration.getActiveShapeType()) {
            case ELLIPSE:
                IShape ellipse = IShapeFactory.createEllipse(startPair, endPair, shapeConfiguration);
                masterShapeList.add(ellipse);
                break;
            case RECTANGLE:
                IShape rectangle = IShapeFactory.createRectangle(startPair, endPair, shapeConfiguration);
                masterShapeList.add(rectangle);
                break;
            case TRIANGLE:
                IShape triangle = IShapeFactory.createTriangle(startPair, endPair, shapeConfiguration);
                masterShapeList.add(triangle);
                break;
            default:
                throw new Error("Draw Shape Commane Error occurred");
        }
        CommandHistory.add(this);
    }

    public void undo() {
        System.out.println("Clicked UNDO after drawing a shape");
        lastShapeDrawn = masterShapeList.getShapeList().get(masterShapeList.getShapeList().size()-1);
        masterShapeList.remove();
    }

    public void redo() {
        System.out.println("Clicked REDO after deleting a shape");
        masterShapeList.add(lastShapeDrawn);
    }
}
