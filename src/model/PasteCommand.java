package model;

import model.interfaces.ICommand;
import model.interfaces.IShape;

public class PasteCommand implements ICommand {
    private ShapeList masterShapeList;
    private ShapeList clipboardShapeList;

    public PasteCommand(ShapeListManager shapeListManager) {
        this.masterShapeList = shapeListManager.getMasterShapeList();
        this.clipboardShapeList = shapeListManager.getClipboardShapeList();
    }

    @Override
    public void run() {
//        System.out.println("Master shape list size BEFORE paste " + masterShapeList.getShapeList().size());
        for (IShape s : clipboardShapeList.getShapeList()) {
            IShape pastedShape = s.deepCopy();
            pastedShape.setStartPair(new Pair(s.getStartPair().getX() + 40, s.getStartPair().getY()));
            pastedShape.setEndPair(new Pair(s.getEndPair().getX() + 40, s.getEndPair().getY()));
            masterShapeList.add(pastedShape);
        }
//        System.out.println("Master shape list size AFTER paste " + masterShapeList.getShapeList().size());
    }
}
