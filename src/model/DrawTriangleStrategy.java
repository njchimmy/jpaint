package model;

import model.interfaces.IDrawShapeStrategy;
import model.interfaces.IShape;

import java.awt.*;

public class DrawTriangleStrategy implements IDrawShapeStrategy {

    DrawTriangleStrategy() {
    }

    @Override
    public void draw(Graphics2D graphics2D, IShape triangle) {
        // TO DO
        // MAKE THIS DECORATOR PATTERN
        int[] xPoints = createXPointsArray(triangle.getStartPair(), triangle.getEndPair());
        int[] yPoints = createYPointsArray(triangle.getStartPair(), triangle.getEndPair());

        switch (triangle.getShapeConfiguration().getActiveShapeShadingType()) {
            case OUTLINE_AND_FILLED_IN:
                graphics2D.setColor(triangle.getShapeConfiguration().getActiveSecondaryColor().getColor());
                graphics2D.fillPolygon(xPoints, yPoints, 3);
                graphics2D.setColor(triangle.getShapeConfiguration().getActivePrimaryColor().getColor());
                graphics2D.setStroke(new BasicStroke(3));
                graphics2D.drawPolygon(xPoints, yPoints, 3);
            case OUTLINE:
                graphics2D.setColor(triangle.getShapeConfiguration().getActivePrimaryColor().getColor());
                graphics2D.setStroke(new BasicStroke(3));
                graphics2D.drawPolygon(xPoints, yPoints, 3);
            default:
                graphics2D.setColor(triangle.getShapeConfiguration().getActiveSecondaryColor().getColor());
                graphics2D.fillPolygon(xPoints, yPoints, 3);
        }
    }

//    public void drawOutline (Graphics2D graphics2D, IShape triangle) {
//        graphics2D.setColor(triangle.getShapeConfiguration().getActivePrimaryColor().getColor());
//        graphics2D.setStroke(new BasicStroke(3));
//        graphics2D.drawPolygon(xPoints, yPoints, 3);
//    }
//
//    public void drawFilledIn {
//
//    }

    private int[] createXPointsArray(Pair startPair, Pair endPair) {
        int[] xPoints = new int[3];
        int xPosition1 = startPair.getX();
        int xPosition2 = endPair.getX();
        xPoints[0] = xPosition1;
        xPoints[1] = xPosition1;
        xPoints[2] = xPosition2;
        return xPoints;
    }

    private int[] createYPointsArray(Pair startPair, Pair endPair) {
        int[] yPoints = new int[3];
        int yPosition1 = startPair.getY();
        int yPosition2 = endPair.getY();
        yPoints[0] = yPosition1;
        yPoints[1] = yPosition2;
        yPoints[2] = yPosition2;
        return yPoints;
    }
}
